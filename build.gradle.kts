plugins {
    id("scala")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.scala-lang:scala-library:2.13.11")
    testImplementation("junit:junit:4.13.1")
    testImplementation("org.scalatest:scalatest_2.12:3.0.5")
    testImplementation("org.scalamock:scalamock_2.12:4.4.0")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:5.8.2")
}
